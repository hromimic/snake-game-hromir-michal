import unittest
import pygame
import time
from unittest.mock import patch
from settings import *
from player import Player, Difficulty, Score
from obstacle import Obstacle

pygame.init()

pygame.quit()
screen = pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])

class TestPlayer(unittest.TestCase):
    """
    Unit tests for the Player class.
    """
    def setUp(self):
        """
        Sets up the Player instance before each test method.
        """
        self.player = Player()

    def test_init(self):
        """
        Tests the initialization of the Player instance.
        Ensures image and rect attributes are correctly set.
        """
        self.assertTrue(isinstance(self.player.image, pygame.Surface))
        self.assertTrue(isinstance(self.player.rect, pygame.Rect))
        self.assertTrue(self.player.rect.x == SCREEN_WIDTH // 2 - 35)
        self.assertTrue(self.player.rect.y == SCREEN_HEIGHT - 2 * PLAYER_HEIGHT)

    @patch('pygame.key.get_pressed')
    def test_move_left(self, mock_get_pressed):
        """
        Tests the player movement to the left.
        Mocks the LEFT key press and verifies the player moves left.
        """
        # Mock the key press for LEFT
        mock_get_pressed.return_value = {pygame.K_LEFT: True, pygame.K_RIGHT: False}
        
        # Store initial position
        initial_x = self.player.rect.x
        
        # Update the player (this should move the player left)
        self.player.update()
        
        # Check if the player has moved left
        self.assertEqual(self.player.rect.x, initial_x - PLAYER_SPEED)
    
    @patch('pygame.key.get_pressed')
    def test_move_right(self, mock_get_pressed):
        """
        Tests the player movement to the right.
        Mocks the RIGHT key press and verifies the player moves right.
        """
        # Mock the key press for RIGHT
        mock_get_pressed.return_value = {pygame.K_LEFT: False, pygame.K_RIGHT: True}
        
        # Store initial position
        initial_x = self.player.rect.x
        
        # Update the player (this should move the player right)
        self.player.update()
        
        # Check if the player has moved right
        self.assertEqual(self.player.rect.x, initial_x + PLAYER_SPEED)
    
    @patch('pygame.key.get_pressed')
    def test_no_movement(self, mock_get_pressed):
        """
        Tests the player with no movement.
        Mocks no key press and verifies the player does not move.
        """
        # Mock no key press
        mock_get_pressed.return_value = {pygame.K_LEFT: False, pygame.K_RIGHT: False}
        
        # Store initial position
        initial_x = self.player.rect.x
        
        # Update the player (this should not move the player)
        self.player.update()
        
        # Check if the player has not moved
        self.assertEqual(self.player.rect.x, initial_x)
    
    @patch('pygame.key.get_pressed')
    def test_screen_limit_left(self, mock_get_pressed):
        """
        Tests the player does not move beyond the left screen limit.
        Mocks the LEFT key press while the player is at the left edge.
        """
        # Move the player to the left edge
        self.player.rect.x = 0
        
        # Mock the key press for LEFT
        mock_get_pressed.return_value = {pygame.K_LEFT: True, pygame.K_RIGHT: False}
        
        # Update the player (this should not move the player beyond the left edge)
        self.player.update()
        
        # Check if the player has not moved beyond the left edge
        self.assertEqual(self.player.rect.x, 0)
    
    @patch('pygame.key.get_pressed')
    def test_screen_limit_right(self, mock_get_pressed):
        """
        Tests the player does not move beyond the right screen limit.
        Mocks the RIGHT key press while the player is at the right edge.
        """
        # Move the player to the right edge
        self.player.rect.x = SCREEN_WIDTH - PLAYER_WIDTH
        
        # Mock the key press for RIGHT
        mock_get_pressed.return_value = {pygame.K_LEFT: False, pygame.K_RIGHT: True}
        
        # Update the player (this should not move the player beyond the right edge)
        self.player.update()
        
        # Check if the player has not moved beyond the right edge
        self.assertEqual(self.player.rect.x, SCREEN_WIDTH - PLAYER_WIDTH)

    def test_load_textures(self):
        """
        Tests the player's texture loading.
        Ensures two textures are loaded and are pygame.Surface instances.
        """
        self.assertEqual(len(self.player.player_textures), 2)
        for texture in self.player.player_textures:
            self.assertIsInstance(texture, pygame.Surface)

class TestObstacle(unittest.TestCase):
    """
    Unit tests for the Obstacle class.
    """
    def setUp(self):
        """
        Sets up the Obstacle instance before each test method.
        """
        #pygame.init()
        self.obstacle = Obstacle()

    def test_init(self):
        """
        Tests the initialization of the Obstacle instance.
        Ensures image and rect attributes are correctly set.
        """
        self.assertIsInstance(self.obstacle.image, pygame.Surface)
        self.assertIsInstance(self.obstacle.rect, pygame.Rect)
        self.assertTrue(0 <= self.obstacle.rect.x <= SCREEN_WIDTH - OBSTACLE_WIDTH)
        self.assertEqual(self.obstacle.rect.y, -OBSTACLE_HEIGHT)

    def test_update(self):
        """
        Tests the obstacle's update method.
        Ensures the obstacle moves downward at the expected speed.
        """
        initial_y = self.obstacle.rect.y
        self.obstacle.update()
        self.assertEqual(self.obstacle.rect.y, initial_y + OBSTACLE_SPEED)

    def test_load_textures(self):
        """
        Tests the obstacle's texture loading.
        Ensures two textures are loaded and are pygame.Surface instances.
        """
        self.assertEqual(len(self.obstacle.obstacle_textures), 2)
        for texture in self.obstacle.obstacle_textures:
            self.assertIsInstance(texture, pygame.Surface)

class TestDifficulty(unittest.TestCase):
    """
    Unit tests for the Difficulty class.
    """
    def setUp(self):
        """
        Sets up the Difficulty instance before each test method.
        """
        self.difficulty = Difficulty()

    def test_init(self):
        """
        Tests the initialization of the Difficulty instance.
        Ensures obstacle frequency and speed are correctly set.
        """
        self.assertEqual(self.difficulty.obstacle_frequency, OBSTACLE_FREQUENCY)
        self.assertEqual(self.difficulty.obstacle_speed, OBSTACLE_SPEED)

    def test_increase_difficulty(self):
        """
        Tests the increase_difficulty method.
        Ensures the obstacle frequency decreases and speed increases as expected.
        """
        # Set initial values
        self.difficulty.obstacle_frequency = 240
        self.difficulty.obstacle_speed = 2
        self.difficulty.start_time = 0
        self.difficulty.increase_interval = 40

        # Call the function
        self.difficulty.increase_difficulty()

        # Check if difficulty increased as expected
        self.assertEqual(self.difficulty.obstacle_frequency, 235)
        self.assertEqual(self.difficulty.obstacle_speed, 4)

    def test_reset_difficulty(self):
        """
        Tests the reset_difficulty method.
        Ensures obstacle frequency and speed are reset to initial values.
        """
        self.difficulty.obstacle_frequency -= 10  # Modify difficulty
        self.difficulty.obstacle_speed += 5
        self.difficulty.reset_difficulty()
        self.assertEqual(self.difficulty.obstacle_frequency, OBSTACLE_FREQUENCY)
        self.assertEqual(self.difficulty.obstacle_speed, OBSTACLE_SPEED)

class TestScore(unittest.TestCase):
    """
    Unit tests for the Score class.
    """
    def setUp(self):
        """
        Sets up the Score instance before each test method.
        """
        self.score = Score()

    def test_init(self):
        """
        Tests the initialization of the Score instance.
        Ensures score and score_time attributes are correctly set.
        """
        self.assertEqual(self.score.score, SCORE)
        self.assertAlmostEqual(self.score.score_time, time.time(), delta=1)  # Allow for slight time difference

    def test_update_score(self):
        """
        Tests the update_score method.
        Simulates time passing and ensures the score updates correctly.
        """
        time.sleep(self.score.update_interval + 0.1)  # Ensure interval has passed
        self.score.update_score()
        self.assertEqual(self.score.score, SCORE + 10)