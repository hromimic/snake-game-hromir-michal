import pygame
import time
import random
from settings import *
from player import Player, Difficulty, Score
from obstacle import Obstacle

"""
SpaceDiver - A simple python game 
where the player controls a character 
diving through space while avoiding obstacles.
"""

pygame.init()

obstacle_frequency_new = OBSTACLE_FREQUENCY

screen = pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])

# app window
pygame.display.set_caption("SpaceDiver")
pygame.display.set_icon(pygame.image.load(ICON).convert_alpha())

# labels
font1 = pygame.font.Font(GENERAL_FONT, FONT_SIZE)
start_label = font1.render("Start", True, (BLUE))
start_label_rect = start_label.get_rect(topleft=(195, 430))
quit_button = font1.render("QUIT", True, (BLUE))
quit_button_rect = quit_button.get_rect(topleft=(196, 530))

# lobby
bg_lobby = pygame.image.load(HUB).convert_alpha()
lobby = pygame.mixer.Sound('sounds/lobby1.mp3')

# background
bg = pygame.image.load(BACKGROUND).convert_alpha()
bg_sounds = [
    pygame.mixer.Sound('sounds/lobby2.mp3'),
    pygame.mixer.Sound('sounds/ambient2.mp3')
]
bg_sound = random.choice(bg_sounds)
bg_x = random.randrange(-100, -20)
bg_y = 0

# sprites
all_sprites = pygame.sprite.Group()
obstacles = pygame.sprite.Group()
player = Player()
obstacle = Obstacle()
difficulty = Difficulty()
SCORE = Score()

start_time = time.time()
score_time = time.time()

while RUNNING: # lobby
    # lobby screen
    lobby.play(fade_ms=30)
    lobby.set_volume(0.4)
    screen.blit(bg_lobby, (0, 0))
    screen.blit(start_label, start_label_rect)
    screen.blit(quit_button, quit_button_rect)
    
    # process events
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            RUNNING = False

    # lobby buttons
    mouse = pygame.mouse.get_pos()
    keys = pygame.key.get_pressed()
    if start_label_rect.collidepoint(mouse) and pygame.mouse.get_pressed()[0] or keys[pygame.K_RETURN]:
        GAMEPLAY = True
    if quit_button_rect.collidepoint(mouse) and pygame.mouse.get_pressed()[0]:
        RUNNING = False

    if GAMEPLAY: # game starts
        
        lobby.stop()
        all_sprites.add(player)

        # background
        screen.blit(bg, (bg_x, bg_y))
        screen.blit(bg, (bg_x, bg_y-700))
        bg_y += 0.3
        if bg_y >= 700:
            bg_y = 0
        
        bg_sound.play(fade_ms=30)
        bg_sound.set_volume(0.3)

        # spawn new obstacles
        for i in range(3, 10):
            if random.randrange(obstacle_frequency_new) == 0:
                obstacle = Obstacle()
                all_sprites.add(obstacle)
                obstacles.add(obstacle)

        # difficulty & score
        difficulty.increase_difficulty()
        SCORE.update_score()
        score_label = font1.render(f"Score: {SCORE.score}", True, WHITE)
        screen.blit(score_label, (10, 10))

        # update sprites
        all_sprites.update()
        all_sprites.draw(screen)
        
        # check collision and reset
        if pygame.sprite.spritecollide(player, obstacles, False) or keys[pygame.K_ESCAPE]: # TODO reset class
            GAMEPLAY = False
            start_time = time.time()
            all_sprites.empty()
            obstacles.empty()
            SCORE.score = 0
            player.rect.x = SCREEN_WIDTH // 2 - 35
            difficulty.reset_difficulty()
            bg_sound.stop()

    # screen update
    pygame.display.flip()

    # FPS
    pygame.time.Clock().tick(FPS)

# Game End
pygame.quit()