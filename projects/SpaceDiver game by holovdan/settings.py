
# colors
WHITE = (255, 255, 255)
BLUE = (0, 0, 34)

# game settings
SCREEN_WIDTH = 480
SCREEN_HEIGHT = 640
FPS = 60
RUNNING = True # lobby
GAMEPLAY = False # game level
GENERAL_FONT = 'fonts/Oswald-Regular.ttf'
FONT_SIZE = 42
ICON = 'textures/player1.png'
BACKGROUND = 'textures/background.png'
HUB = 'textures/logo1.png'

# player settings
PLAYER_WIDTH = 50
PLAYER_HEIGHT = 50
PLAYER_SPEED = 5
SCORE = 0
SCORE_UPDATE_INTERVAL = 5

# obstacle settings
OBSTACLE_WIDTH = 50
OBSTACLE_HEIGHT = 50
OBSTACLE_SPEED = 2
OBSTACLE_FREQUENCY = 240
DIFFICULTY_INCREASE_INTERVAL = 40