import pygame
import time
import random
from settings import *

class Player(pygame.sprite.Sprite):
    """
    Player class for controlling the game character.

    Attributes:
        player_textures (list): List of player textures.
        rect (pygame.Rect): Rectangular area representing the player's position.
    """
    def __init__(self) -> None:
        """
        Initialize the player object.
        """
        super().__init__()
        self.load_textures()
        self.image = random.choice(self.player_textures)
        self.rect = self.image.get_rect()
        self.rect.x = SCREEN_WIDTH // 2 - 35
        self.rect.y = SCREEN_HEIGHT - 2 * PLAYER_HEIGHT
    
    def update(self) -> None:
        """
        Update the player's position based on settings.
        
        Controls:
            LEFT: Move left.
            RIGHT: Move right.
        """
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            self.rect.x -= PLAYER_SPEED
        elif keys[pygame.K_RIGHT]:
            self.rect.x += PLAYER_SPEED

        # player screen limits
        if self.rect.x < 0:
            self.rect.x = 0
        elif self.rect.x > SCREEN_WIDTH - PLAYER_WIDTH:
            self.rect.x = SCREEN_WIDTH - PLAYER_WIDTH

    def load_textures(self) -> None:
        """
        Load player textures from textures file.
        """
        self.player_textures = [
            pygame.image.load('textures/player1.png').convert_alpha(),
            pygame.image.load('textures/player2.png').convert_alpha()
        ]

class Difficulty:
    """
    Class for managing game difficulty settings.

    Attributes:
        obstacle_frequency (int): Updated obstacle frequency.
        obstacle_speed (int): Updated obstacle speed.
        increase_interval (int): Time interval for increasing difficulty.
        start_time (float): Time when the difficulty increase starts.
    """
    def __init__(self, 
                 obstacle_frequency=OBSTACLE_FREQUENCY, 
                 obstacle_speed=OBSTACLE_SPEED, 
                 increase_interval=DIFFICULTY_INCREASE_INTERVAL
                 ) -> None:
        """
        Initialize the Difficulty object.

        Args:
            obstacle_frequency (int): New obstacle frequency.
            obstacle_speed (int): New obstacle speed.
            increase_interval (int): Interval for increasing difficulty.
        """
        self.initial_obstacle_frequency = obstacle_frequency
        self.initial_obstacle_speed = obstacle_speed
        self.obstacle_frequency = obstacle_frequency
        self.obstacle_speed = obstacle_speed
        self.increase_interval = increase_interval
        self.start_time = time.time()

    def increase_difficulty(self) -> None:
        """
        Increase game difficulty based on a predefined interval.
        """
        if time.time() - self.start_time > self.increase_interval:
            self.obstacle_frequency -= 5
            self.obstacle_speed += 2
            self.start_time = time.time()

    def reset_difficulty(self) -> None:
        """
        Reset the game difficulty to its initial values.
        """
        self.obstacle_frequency = self.initial_obstacle_frequency
        self.obstacle_speed = self.initial_obstacle_speed

class Score:
    """
    Class for managing game score.

    Attributes:
        score (int): Current score
        score_time (float): Time when the score was last updated.
        update_interval (int): Time interval for updating the score.
    """
    def __init__(self, 
                 initial_score=SCORE, 
                 update_interval=SCORE_UPDATE_INTERVAL
                 ) -> None:
        """
        Initialize the Score object.

        Args:
            initial_score (int): Initial score value.
            update_interval (int): Interval for updating the score.
        """
        self.score = initial_score
        self.score_time = time.time()
        self.update_interval = update_interval

    def update_score(self) -> None:
        """
        Update the game score based on a predefined interval.
        """
        if time.time() - self.score_time > self.update_interval:
            self.score += 10
            self.score_time = time.time()