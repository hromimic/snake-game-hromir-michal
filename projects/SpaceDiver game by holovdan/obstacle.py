import pygame
import random
from settings import *

class Obstacle(pygame.sprite.Sprite):
    """
    Obstacle class representing obstacles that the player must avoid.

    Attributes:
        obstacle_textures (list): List of obstacle textures.
        rect (pygame.Rect): Rectangular area representing the obstacle's position (hitbox).
    """
    def __init__(self) -> None:
        """
        Initialize the obstacle object.
        """
        super().__init__()
        self.load_textures()
        self.image = random.choice(self.obstacle_textures)
        self.rect = self.image.get_rect()
        self.rect.x = random.randrange(0, SCREEN_WIDTH - OBSTACLE_WIDTH)
        self.rect.y = -OBSTACLE_HEIGHT

    def update(self) -> None:
        """
        Update the position of the obstacle.
        """
        self.rect.y += OBSTACLE_SPEED
        
    def load_textures(self) -> None:
        """
        Load obstacle textures from texture file.
        """
        self.obstacle_textures = [
            pygame.image.load('textures/obstacle1.png').convert_alpha(),
            pygame.image.load('textures/obstacle2.png').convert_alpha()
        ]