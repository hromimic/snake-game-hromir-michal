import numpy as np
import matplotlib.pyplot as plt
class DataLoader:
    def __init__(self, filepath):
        self.filepath = filepath
        self.data = None
    def load_data(self):
        self.data = np.genfromtxt(self.filepath, delimiter=';', skip_header=1)
        return self.data
    def get_column_index(self, column_name):
        headers = np.genfromtxt(self.filepath, delimiter=';', max_rows=1, dtype=str)
        return np.where(headers == column_name)[0][0]
class DataAnalyzer:
    def __init__(self, data):
        self.data = data
    def mean(self, column):
        return np.mean(self.data[:, column])
    def median(self, column):
        return np.median(self.data[:, column])
    def variance(self, column):
        return np.var(self.data[:, column])
    def std_dev(self, column):
        return np.std(self.data[:, column])
    def help():
        print("Dostupné analýzy:")
        print("- mean: Spočítá průměrnou hodnotu Vámi vybraného parametru.")
        print("- median: Vrátí prostřední hodntu Vámi vybraného parametru.")
        print("- variance: Spočítá rozptyl hodnot Vámi vybraného parametru.")
        print("- std_dev: Spočítá rozložení hodnot Vámi vybraného parametru.")
class DataVisualizer:
    def __init__(self, data):
        self.data = data
    def plot_histogram(self, column, bins=10, title="Histogram"):
        plt.hist(self.data[:, column], bins=bins)
        plt.title(title)
        plt.xlabel(f'Column {column}')
        plt.ylabel('Frequency')
        plt.show()
if __name__ == "__main__":
    loader = DataLoader('winequality-red.csv')
    data = loader.load_data()
    variable_names = [f"Variable {i+1}: {header}" for i, header in enumerate(np.genfromtxt('winequality-red.csv', delimiter=';', max_rows=1, dtype=str))]
    print("Parametry na výběr:")
    for variable_name in variable_names:
        print(variable_name)
    while True:
        column_name = input("Vyberte parametr na analýzu: ")
        try:
            column_index=loader.get_column_index(column_name)
            break
        except KeyError:
             print("Tento parametr není z výběru.")
    analysis_options = ['mean', 'median', 'variance', 'std_dev', 'help']
    print("\nDostupné analýzy:")
    for option in analysis_options:
        print(option)
    while True:
        analysis_type = input("Zvolte typ analýzy (mean/median/variance/std_dev) nebo help pro vysvětlivky: ").lower()
        if analysis_type == 'help':
            DataAnalyzer.help()    
        elif analysis_type in ['mean', 'median', 'variance', 'std_dev']:
            analysis_method = getattr(DataAnalyzer(data), analysis_type)
            result = analysis_method(column_index)
            print(f"{analysis_type.capitalize()} of {column_name}: {result}")
            break
        else:
            print("Vámi zadaný typ nenalezen. Prosím vyberte z 'mean', 'median', 'variance', 'std_dev', nebo 'help'.")
    visualizer = DataVisualizer(data)
    visualizer.plot_histogram(column_index, title=f"Histogram of {column_name}")