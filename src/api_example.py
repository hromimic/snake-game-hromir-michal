import uvicorn
from fastapi import FastAPI

app = FastAPI()

@app.get("/greet")
async def greet(name: str = "World") -> dict:
    return {"message": f"Hello, {name}!"}

@app.get("/square")
async def square(number: int = 1) -> dict:
    return {"result": number * number}

if __name__ == "__main__":    
    uvicorn.run(app, host="127.0.0.1", port=8000)
